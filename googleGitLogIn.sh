#!/bin/bash

if [[ "$1" == "--org" ]]; then
	xdg-open https://accounts.google.com/signupwithoutgmail
elif [[ "$1" == "--gpg"]]; then
	xdg-open https://developers.google.com/widevine/pgp
fi

xdg-open https://www.googlesource.com/new-password
