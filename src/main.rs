use waddaFork::tait;

#[tokio::main]
async fn main() -> Result<()> {
    // create ethers client and wrap it in Arc<M>
    //  let client = Arc::new(nick.local.CLIENT);
    await tait().?;
    println!("Hello, world!");
}
